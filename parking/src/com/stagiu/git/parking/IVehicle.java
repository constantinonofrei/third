package com.stagiu.git.parking;

public interface IVehicle {
	public Double calculateCost();
	public void drive();
}
